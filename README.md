# High Level Docs
[Confluence|https://??]

# Processes
All python scripts require a python virtualenv by running.

`cd ./bin && make ve && source ./ve/bin/activate`

## Adding source shares.
1. Run `audit-volumes.py`to  examine current status of source shares.

# Sub-Directories Involved
| Subdir | Purpose |
| ------------- | ------------- |
| bin | Location of all binaries.  |
| etc | Location of all config files. |

# Configuration
* `etc/komprise-credentials.cfg` - Default configuration and credentials file name.
    
# Scripts/Files
Scripts are located in the 'bin' directory.

## Building and Testing
* `Makefile` - Makefile for building/destroying the python virtualenv environment.
* `test-config.py` - Test methods in komprise/config.py
* `create-config.py` - Create a config file.  Defaults to `etc/komprise-credentials.cfg`.
* `verify-director.py` - Verify connectivity of the director and credentials within the config file.
* `show-stat-structures.py` - Show all the statistics structures.

## Audit
* `volumes-audit.py` - Provide status of current volumes in Komprise - volume IDs, path, whether analyzed or in plan, etc.
* `plan-audit.py` - List status of Komprise plans.

## Performing Tasks
* `make-vol-symlinks.py` - Create symlinks which are similar to source shares on `/locus/data/` which point to Komprise S3 target IDs - i.e. `/kt/`.
* `volumes-analyze.py` - Given a list of volume IDs.  Analyze the volumes.

## Libraries
* `komprise/config.py` - Class for managing the Komprise configuration file.
* `komprise/session.py` - Class for managing a session with a Komprise director.

* `komprise/basestats.py` - Parent class managing Komprise statistics.
* `komprise/filerstats.py` - Filer statistics.
* `komprise/groupstats.py` - Group statistics.
* `komprise/planstats.py` - Plan statistics.
* `komprise/volstats.py` - Volume statistics.

# To Do


