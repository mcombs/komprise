#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/volumes-analyze.py
#=============================================================================#
#
import sys
import os
import argparse
import json
import re
import komprise
#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-2])
#
#=============================================================================#
def main():
    parser = argparse.ArgumentParser(description='Analyze Komprise volumes.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
    parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
    parser.add_argument('--noop', action='store_true', default=False, help='Perform no operation.')
    parser.add_argument('--force', action='store_true', default=False, help='Force re-analysis.')
    parser.add_argument('--asynchronous', action='store_true', default=False, help='Analyze asynchronously.')
    parser.add_argument('volids', action='store', nargs='+', default=False, help='Overwrite existing links.')
    args = parser.parse_args()
    s = VolumesAnalyze(args=args)
    s.setup()
    #
    # Now sanity test the volume IDs passed to ensure they exist or 
    # whether they have already been analyzed.
    s.sanity_tests(args)
    s.start_analysis(args.volids)
    return(s.exit_status)
#
#=============================================================================#
class VolumesAnalyze(object):
    '''Analyze Komprise volumes.

    Parameters:
       debug        (OPT: bool)    : Enable debug mode. Sets log level to 'debug'.
       loglevel     (OPT: str)     : Set logging level
       noop         (OPT: bool)    : Do nothing.
       force        (OPT: bool)    : Force analysis even if one has already happened.
       asynchronous (OPT: bool)    : Analyze asynchronously.

    Methods:
       setup           : Setup connections to a Komprise observer.
       sanity_tests    : Run any sanity tests before starting work.
       start_analysis  : Start the analysis of a series of volumes.

    '''
    #
    #
    #-------------------------------------------------------------------------#
    def __init__(self, args=None):
        self.exit_status = 0
        self.debug = args.debug
        self.loglevel = args.loglevel.upper()
        mylog = komprise.log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        self.noop = args.noop
        self.force = args.force
        self.asynchronous = args.asynchronous
        self.volids = args.volids
        #
        # Variables associated with the Komprise.
        self.filer = None                  # <-- Filer class.
        self.vol = None                    # <-- Volume class.
        #
        # Internal variables.
        # https://invitae.komprise.com/api/v1/volume/analyze?async=false
        self.komp_sess = None              # <-- Komprise session
        self.url = '/api/v1/volume/analyze?async={}'.format(str(args.asynchronous).lower())
        self.vols_analyzed = None      # <-- Volumes analyzed.
        self.vols_not_analyzed = None  # <-- Volumes not analyzed.
        return
    #
    #
    #-------------------------------------------------------------------------#
    def setup(self):
        '''Set up dependencies before running.
        '''
        self.log.info('Running setup')
        self.komp_sess = komprise.session.Session(debug=self.debug, loglevel=self.loglevel)
        self.komp_sess.connect()
        self.filer = komprise.filerstats.FilerStats(debug=self.debug, loglevel=self.loglevel, session=self.komp_sess)
        self.filer.build_filers()
        self.vol = komprise.volstats.VolStats(debug=self.debug, loglevel=self.loglevel, session=self.komp_sess)
        # Specify the exact filer otherwise stats for all filers are built.
        for filerid in self.filer.get_dict().keys():
            self.vol.build_volumes(filerid=filerid)
        self.vols_not_analyzed = self.vol.get_dict(analyzed=False)
        self.vols_analyzed = self.vol.get_dict(analyzed=True)
        return
    #
    #
    #-------------------------------------------------------------------------#
    def sanity_tests(self, args=None):
        '''Run sanity tests.

        Args:
           args       (REQ: class) : An argparse class of arguments.

        Returns:
           None.

        '''
        self.log.info('Running sanity tests.')
        for volid in args.volids:
            if volid in self.vols_analyzed.keys() and self.force == False:
                raise ValueError('Volume "{}" with ID "{}" has already been analyzed but "--force" is not specified.'.format(self.vol.get('volume', volid, 'path'), volid))
            elif volid not in self.vols_not_analyzed.keys() and volid not in self.vols_analyzed.keys():
                raise ValueError('Volume ID "{}" is not known by Komprise.'.format(volid))
        return
    #
    #
    #-------------------------------------------------------------------------#
    def start_analysis(self, volids=None):
        '''Start analysis of the volumes.

        Args:
           volids     (REQ: class) : List of volume IDs to analyze.

        Returns:
           None.

        '''
        msg = 'Initiating analysis of the following volumes:\n'
        msg += '{:<12s} {:<}\n'.format('Volume ID', 'Path')
        msg += '{:<12s} {:<}\n'.format('-'*12, '-'*40)
        for volid in volids:
            msg += '{:<12s} {:<}\n'.format(volid, self.vol.get(volid, 'path'))
        self.log.info(msg)
        path=self.url
        body = { 'volumeIds' : volids }
        if self.noop == True:
            return
        self.komp_sess.post(path=path, body=body)
        return
#
#
#=============================================================================#
if __name__ == '__main__':  sys.exit(main())
