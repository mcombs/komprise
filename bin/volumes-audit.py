#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/volumes-audit.py
#=============================================================================#
#
import sys
import os
import argparse
import json
import re
import komprise
#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-2])
#
#=============================================================================#
def main():
    parser = argparse.ArgumentParser(description='Create Komprise symlinks.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
    parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
    parser.add_argument('--onlyerrors', action='store_true', default=False, help='Show only errors.')
    args = parser.parse_args()
    s = VolumesAudit(args=args)
    s.setup()
    if args.onlyerrors == False:
        for filerid in s.filer.get_dict().keys():
            print('{}'.format(s.format_stats(filerid)))
    print('{}'.format(s.format_errors()))
    return(s.exit_status)
#
#=============================================================================#
class VolumesAudit(object):
    '''Create Komprise symlinks to their mapped id number.

    Parameters:
       debug      (OPT: bool)    : Enable debug mode. Sets log level to 'debug'.
       loglevel   (OPT: str)     : Set logging level [DEF: 'info']
    '''
    def __init__(self, args=None):
        self.exit_status = 0
        self.debug = args.debug
        if self.debug == True: args.loglevel = 'DEBUG'
        self.loglevel = args.loglevel.upper()
        mylog = komprise.log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        self.onlyerrors = args.onlyerrors
        #
        # Internal variables.
        # https://invitae.komprise.com/api/v1/volume/analyze?async=false
        self.url = '/api/v1/plans/active'
        self.filer = None                  # <-- Filer class.
        self.vol = None                    # <-- Volume class.
        return
    #
    #
    #-------------------------------------------------------------------------#
    def setup(self):
        '''Set up dependencies before running.
        '''
        self.log.info('Running setup')
        komp_sess = komprise.session.Session(debug=self.debug, loglevel=self.loglevel)
        komp_sess.connect()
        self.filer = komprise.filerstats.FilerStats(debug=self.debug, loglevel=self.loglevel, session=komp_sess)
        self.filer.build_filers()
        self.vol = komprise.volstats.VolStats(debug=self.debug, loglevel=self.loglevel, session=komp_sess)
        # Specify the exact filer otherwise stats for all filers are built.
        for filerid in self.filer.get_dict().keys():
            self.vol.build_volumes(filerid=filerid)
        return
    #
    #
    #-------------------------------------------------------------------------#
    def format_stats(self, filerid=None):
        '''Print the results of the audit.
        Args:
           None.

        Returns:
           None.
        '''
        printout = '{}\nVolumes for filer\n'.format('='*77)
        printout += '    {:<25}  :  {:<}\n'.format('Site ID', self.filer.get(filerid, 'site_id'))
        printout += '    {:<25}  :  {:<}\n'.format('Filer ID', filerid)
        printout += '    {:<25}  :  {:<}\n'.format('IP Address', self.filer.get(filerid, 'ipaddress'))
        printout += '    {:<25}  :  {:<}\n'.format('Total Volumes', self.filer.get(filerid, 'total_volumes'))
        printout += '    {:<25}  :  {:<}\n'.format('Analyzed Volumes', self.filer.get(filerid, 'analyzed_volumes'))
        printout += '    {:<25}  :  {:<}\n'.format('Volumes with Warnings', self.filer.get(filerid, 'warn_volumes'))
        printout += '    {:<25}  :  {:<}\n'.format('Volumes with Errors', self.filer.get(filerid, 'error_volumes'))
        printout += '\n{:<10s}  {:<10s}  {:<8s}  {:<8s}  {:<}\n'.format('VolID', 'Run Status', 'Analyzed', 'In Plan', 'Path')
        printout += '{:<10s}  {:<10s}  {:<8s}  {:<8s}  {:<}\n'.format('-'*10, '-'*10, '-'*8, '-'*8, '-'*40)
        volstats = self.vol.get_dict(filerid=int(filerid))
        for volid in volstats.keys():
            printout += '{:<10s}  {:<10s}  {:<8s}  {:<8s}  {:<}\n'.format(volid, 
                                                                        volstats[volid]['run_status'], 
                                                                        str(volstats[volid]['analyzed']), 
                                                                        str(volstats[volid]['in_plan']), 
                                                                        volstats[volid]['path'])
        printout += '\n'
        return printout
    #
    #
    #-------------------------------------------------------------------------#
    def format_errors(self):
        error_volumes = self.vol.get_dict(status='ERROR')
        printout = 'No errors to report.'
        if error_volumes:
            printout = '{}\nVolumes with errors\n'.format('='*77)
            printout += '{:<10s}  {:<s}\n'.format('VolID', 'Details')
            printout += '{:<10s}  {:<s}\n'.format('-'*10, '-'*40)
            for volid in error_volumes.keys():
                printout += '    {:<10s}  {:<s}\n'.format(volid, self.vol.get(volid, 'error_details'))
        return printout
#
#
#=============================================================================#
if __name__ == '__main__':  sys.exit(main())

