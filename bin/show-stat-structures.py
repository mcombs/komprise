#!/usr/bin/env python3
#
import os
import komprise
#
#
#=============================================================================#
os.environ['PAGER'] = 'cat'
filer = komprise.filerstats.FilerStats()
filer.show_structure()
print('{}'.format(help(filer.build_filers)))


print('\nVolume statistics requires runnig komprise.filerstats.FilerStats.build_filers() first')
vol = komprise.volstats.VolStats()
vol.show_structure()
print('{}'.format(help(vol.build_volumes)))

plan = komprise.planstats.PlanStats()
plan.show_structure()
print('{}'.format(help(plan.build_plans)))

print('\nGroup statistics can be passed a reply from komprise.planstats.PlanStats.build_plans().')
group = komprise.groupstats.GroupStats()
group.show_structure()
print('{}'.format(help(group.build_groups)))

print('\nAll statistics use the parent class komprise.basestats.BaseStats')
base = komprise.basestats.BaseStats()
print('{}'.format(help(base)))

