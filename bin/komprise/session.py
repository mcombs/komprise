#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/session.py
#=============================================================================#
#
import sys
import os
import socket
import requests
import json
import base64
import logging
from . import log, config
#
#
#=============================================================================#
class Session(object):
    '''Manage a Komprise API session.

    Attributes:
        debug     (OPT: bool) : Enable debug mode.
        loglevel  (OPT: str)  : Log level to use.
        config    (REQ: dict) : Dictionary with configuration parameters as
                                read from the configuration file.
    
    Methods:
        log_api_call          : Log details of the request and response.
        connect               : Connect to - get a token from - the director.
        disconnect            : Disconnect from - remove token - the director.
        get                   : HTTP GET to the director.
        post                  : HTTP POST to the director.
        delete                : HTTP DELETE to the director.
    '''
    #
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO', configfile=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        confclass = config.Config(debug=debug, loglevel=self.loglevel)
        if configfile:
            confclass.set_filename(configfile)
        self.cfg = confclass.read_config()
        self.komprise_url = "https://" + self.cfg['clustername'] + ':443'
        self.session_path = '/api/token/login'
        self.headers={"Content-type": "application/json"}
        self.test_connection = None
    #
    #
    #----------------------------------------------------------------------#
    def _clean_password(self, d=None):
        if isinstance(d, str):
            try:
                old = json.loads(d)
            except:
                return d
        elif isinstance(d, dict):
            old = d
        else:
            return d
        new = {}
        for k, v in old.items():
            if isinstance(v, dict):
                v = self._clean_password(d=v)
            if k == 'password':
                new[k] = '**REDACTED**'
            else:
                new[k] = v
        return new
    #
    #
    #----------------------------------------------------------------------#
    def log_api_call(self, r):
        self.log.debug('''
        ======================================== API Call ========================================
        Request Method   : {}
        Request URL      : {}
        HTTP Code        : {}
        Request Headers  : {}
        Request Body     : {}
        
        Response Headers : {}
        Response Body    : {}
        ==========================================================================================
        '''.format(r.request.method, r.request.url, r.status_code, r.request.headers, self._clean_password(d=r.request.body), r.headers, (r.text.strip()) ))
    #
    #
    #----------------------------------------------------------------------#
    def connect(self, username=None, password=None):
        '''Connect using credentials provided.

        Args:
           username   (REQ: str)   : Username.
           password   (REQ: str)   : Password.

        '''
        self.log.info('Starting API session on {}'.format(self.cfg['clustername']))
        if username == None: username = self.cfg['username']
        if password == None: password = self.cfg['password']
        id_n_refreshtoken = self._login(username=username, password=password)
        self._get_token(body=id_n_refreshtoken)
        self.password = '**REDACTED**'
        self.log.debug('Testing API session for {}'.format(self.cfg['clustername']))
        self.test_connection = self.get(path='/api/v1/dashboard/summary')
        if self.test_connection.status_code == 200:
            self.log.debug('Session established for {}'.format(self.komprise_url))
            return True
        else:
            self.log.error('Failed to establish a connection to {}'.format(self.komprise_url))
            return False
        return False
    #
    #
    #----------------------------------------------------------------------#
    def _login(self, username=None, password=None):
        '''Add the 'id' and 'refreshToken' headers to requests.
        '''
        self.log.debug('Logging into {}'.format(self.komprise_url))
        path = '/api/token/login'
        body = {"email": username,
                "password": password}
        try:
            '''
            Be VERY CAREFUL sending results from this reply
            to log_api_call without self._clean_password().
            '''
            orig_debug_setting = self.debug
            # self.debug = False
            reply = self.post(path=path, body=body)
            #self.log_api_call(r=reply)
            self.debug = orig_debug_setting
        except:
            msg = 'Failed to login to {}'.format(self.komprise_url + path)
            self.log.error(msg)
            body = self._clean_password(d=body)
            password = '**REDACTED**'
            raise Exception(msg)
        if reply.status_code != 200:
            self.log.critical('No status of 200 returned when initially attempting to authenticate.')
            body = self._clean_password(d=body)
            password = '**REDACTED**'
        body = self._clean_password(d=body)
        password = '**REDACTED**'
        json_data = json.loads(reply.text)
        #for key in json_data:
        #    self.headers[str(key)] = str(json_data[key])
        return json.loads(reply.text)
    #
    #
    #----------------------------------------------------------------------#
    def _get_token(self, body=None):
        self.log.debug('Getting a token from {} and adding it to all headers.'.format(self.komprise_url))
        path='/api/token/api'
        try:
            reply = self.post(path=path, body=body)
        except:
            msg = 'Failed to get token from {}'.format(self.komprise_url + path)
            self.log.error(msg)
            raise Exception(msg)
        if reply.status_code != 200:
            msg = 'No status of 200 returned when getting a token.'
            self.log.critical(msg)
            raise Exception(msg)
        json_data = json.loads(reply.text)
        #auth_header = base64.b64encode(str(json_data['token']) + "; type=site; id=" + str(json_data['id']))
        auth_string = str(json_data['token']) + "; type=site; id=" + str(json_data['id'])
        auth_header = base64.b64encode(auth_string.encode('utf-8')).decode()
        self.headers['Authorization'] = 'Basic ' + str(auth_header)
        return
    #
    #
    #----------------------------------------------------------------------#
    def get(self, path=None):
        '''GET the path provided.

        Args:
           path       (REQ: str)   : Path.

        '''
        self.log.info('GET {}'.format(self.komprise_url + path))
        if self.debug == False:
            logging.captureWarnings(True)
        reply = requests.get(self.komprise_url + path,
                             headers=self.headers,
                             verify=self.cfg['verify_ssl'],
                             timeout=self.cfg['timeout'])
        if self.debug: self.log_api_call(r=reply)
        if self.debug == False:
            logging.captureWarnings(False)
        return reply
    #
    #
    #----------------------------------------------------------------------#
    def post(self, path=None, body=None):
        '''POST data to the path provided.

        Args:
           path       (REQ: str)   : Path.
           body       (REQ: dict)  : Dictionary of header data.

        '''
        self.log.info('POST {}'.format(self.komprise_url + path))
        if self.debug == False:
            logging.captureWarnings(True)
        reply = requests.post(self.komprise_url + path,
                              headers=self.headers,
                              data=json.dumps(body),
                              verify=self.cfg['verify_ssl'],
                              timeout=self.cfg['timeout'])
        if self.debug: self.log_api_call(r=reply)
        body = self._clean_password(d=body)
        if self.debug == False:
            logging.captureWarnings(False)
        return reply
    #
    #
    #----------------------------------------------------------------------#
    def delete(self, path=None):
        '''Send DELETE to the path provided.

        Args:
           path       (REQ: str)   : Path.

        '''
        self.log.info('DELETE {}'.format(self.komprise_url + path))
        if self.debug == False:
            logging.captureWarnings(True)
        reply = requests.delete(self.komprise_url + path, 
                                headers=self.headers,
                                verify=self.cfg['verify_ssl'],
                                timeout=self.cfg['timeout'])
        if self.debug: self.log_api_call(r=reply)
        if self.debug == False:
            logging.captureWarnings(False)
        return reply
#
#=============================================================================#
