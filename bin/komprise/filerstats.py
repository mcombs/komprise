#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/filerstats.py
#=============================================================================#
#
import os
import sys
import re
import json
from . import log, basestats
#
#=============================================================================#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-3])
#
#-----------------------------------------------------------------------------#
# Filer statistics.
# FilerID is the key.
FILER_INIT_STATS = {
    'ipaddress'        : '',
    'site_id'          : 0,
    'volume_ids'       : [],
    'total_volumes'    : 0,
    'analyzed_volumes' : 0,
    'warn_volumes'     : 0,
    'error_volumes'    : 0,
}
#
FILER_ALLOWED_VALUES = None
#
#
#=============================================================================#
class FilerStats(basestats.BaseStats):
    '''Komprise filer statistics.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level to use.
        session  (OPT: class)  : Session class with Komprise.

    Methods:
        build_filers           : Build internal stats for the given filer ID.

    '''
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO', session=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Initialize the parent statistics class.  The following are
        # handled here:
        #
        #  - Komprise director session.
        super().__init__(debug=self.debug, 
                         loglevel=self.loglevel, 
                         struct_name='filer', 
                         initial_values=FILER_INIT_STATS, 
                         allowed_values=FILER_ALLOWED_VALUES)
        # Confirm whether we were passed session information or will we
        # need to get a session with Komprise.
        if session:
            try:
                self.ksess = session
                self.ksess.get('/api/v1/dashboard/summary')
            except:
                self._init_session()
        return
    #
    #
    #-------------------------------------------------------------------------#
    def build_filers(self):
        '''Build site stats.

        Args:
           None.

        Returns:
           None.

        #---------------------------------------------------------------------#
        Parsing: https://invitae.komprise.com/api/v1/datastore/site

          "sites": [
            {
              "id": "1004",
              "filers": [
                {
                  "id": "1009",
                  "ipaddress": "nfspool.clvr.locusdev.net",
                  "totalAnalyzedVolumes": 55,
                  "totalNumberOfVolumes": 55,
                  "numberOfVolumesWithErrors": 0,
                  "numberOfVolumesWithWarnings": 0
                },
              ],
            }
        #---------------------------------------------------------------------#
        '''
        self.log.info('Building site and filer stats.')
        if self.ksess == None:  self._init_session()
        reply = json.loads(self.ksess.get('/api/v1/datastore/site').text)
        if len(reply['sites']) > 1:
            self.log.warning('There are more than one site ID "{}".'.format(reply['sites']))
        for site in reply['sites']:
            siteid = str(site['id'])
            #
            # Initialize the site and filer IDs and add values.
            filerids = []
            for filer in site['filers']:
                filerid = str(filer['id'])
                filerids.append(filerid)
                filerinfo = self.init(filerid)
                filerinfo.update({ 'ipaddress'        : str(filer['ipaddress'])                    })
                filerinfo.update({ 'site_id'          : int(siteid)                                })
                filerinfo.update({ 'total_volumes'    : int(filer['totalNumberOfVolumes'])         })
                filerinfo.update({ 'analyzed_volumes' : int(filer['totalAnalyzedVolumes'])         })
                filerinfo.update({ 'warn_volumes'     : int(filer['numberOfVolumesWithWarnings'])  })
                filerinfo.update({ 'error_volumes'    : int(filer['numberOfVolumesWithErrors'])    })
        return
#
#=============================================================================#
