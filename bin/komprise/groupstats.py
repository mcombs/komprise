#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/planstats.py
#=============================================================================#
#
import os
import sys
import re
import json
from . import log, basestats
#
#=============================================================================#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-3])
#
#-----------------------------------------------------------------------------#
# Group statistics.
# Group ID is the key.
GROUP_INIT_STATS = {
    'name'             : '',
    'volume_ids'       : [],
}
#
GROUP_ALLOWED_VALUES = None
#
#
#=============================================================================#
class GroupStats(basestats.BaseStats):
    '''Komprise group statistics.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level to use.
        session  (OPT: class)  : Session class with Komprise.


    Methods:
        set_filename           : Set the filename for the configuration file.

    '''
    #
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO', session=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Initialize the parent statistics class.  The following are
        # handled here:
        #
        #  - Komprise director session.
        super().__init__(debug=self.debug,
                         loglevel=self.loglevel,
                         struct_name='group',
                         initial_values=GROUP_INIT_STATS,
                         allowed_values=GROUP_ALLOWED_VALUES
                         )
        #
        # Confirm whether we were passed session information or will we
        # need to get a session with Komprise.
        if session:
            try:
                self.ksess = session
                self.ksess.get('/api/v1/dashboard/summary')
            except:
                self._init_session()
        return
    #
    #
    #----------------------------------------------------------------------#
    def build_groups(self, reply=None):
        '''Build plan stats.

        Args:
           reply      (REQ: json)  : JSON response of the format below.

        Returns:
           None.

        #---------------------------------------------------------------------#
        Parsing: https://invitae.komprise.com/api/v1/plans

        [
          {
            "groups": [
              {
                "id": "1239-5",
                "name": "analysis_100k_plus",
                "sites": [
                  {
                    "structId": "1239-5-1004",
                    "id": "1004",
                    "volumes": [
                      {
                        "id": "81564",
                        "mountpoint": "nfs://nfspool.clvr.locusdev.net/ifs/locus/data/analysis_results/100000",
        '''
        self.log.info('Building group stats for all plans.')
        if reply == None:
            if self.ksess == None:  self._init_session()
            reply = json.loads(self.ksess.get('/api/v1/plans').text)
        for plan in reply:
            for group in plan['groups']:
                groupid = str(group['id'])
                groupinfo = self.init(groupid)
                groupinfo.update  ({ 'name'       : str(group['name']) })
                for site in group['sites']:
                    volume_ids = []
                    for volume in site['volumes']:
                        volid = str(volume['id'])
                        volume_ids.append(volid)
                groupinfo.update  ({ 'volume_ids' : volume_ids })
        return
#
#
#=============================================================================#
