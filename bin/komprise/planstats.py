#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/planstats.py
#=============================================================================#
#
import os
import sys
import re
import json
from . import log, basestats
#
#=============================================================================#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-3])
#
#-----------------------------------------------------------------------------#
# Plan statistics.
# Plan ID is the key.
PLAN_INIT_STATS = {
    'name'             : '',
    'active'           : False,
    'run_status'       : '',
    'group_ids'        : [],
}
#
PLAN_ALLOWED_VALUES = { 
    'active'           : [ True, False ],
}
#
#=============================================================================#
class PlanStats(basestats.BaseStats):
    '''Komprise plan statistics.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level to use.
        session  (OPT: class)  : Session class with Komprise.


    Methods:
        build_plans            : Build a list of plans.
    '''
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO', session=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Initialize the parent statistics class.  The following are
        # handled here:
        #
        #  - Komprise director session.
        super().__init__(debug=self.debug,
                         loglevel=self.loglevel,
                         struct_name='plan',
                         initial_values=PLAN_INIT_STATS,
                         allowed_values=PLAN_ALLOWED_VALUES
                         )
        # Confirm whether we were passed session information or will we
        # need to get a session with Komprise.
        if session:
            try:
                self.ksess = session
                self.ksess.get('/api/v1/dashboard/summary')
            except:
                self._init_session()
        return
    #
    #
    #-------------------------------------------------------------------------#
    def build_plans(self):
        '''Build plan stats.

        Args:
           None

        Returns:
           reply                   : Returns the reply for processing by
                                     groupstats.GroupStats.build_groups()

        #---------------------------------------------------------------------#
        Parsing: https://invitae.komprise.com/api/v1/plans
        [
          {
            "id": "1239",
            "name": "dr_and_lifecycle_plan_20160927",
            "activeRunExists": true,
            "activeRunInProgress": true,
            "editOfActivePlan": false,
            "active": false,
            "runStatus": "RUNNING",
            "groups": [
              {
                "id": "1239-5",
                "name": "analysis_100k_plus",
                "sites": [
                  {
                    "structId": "1239-5-1004",
                    "id": "1004",
                    "volumes": [
                      {
                        "id": "81564",
                        "mountpoint": "nfs://nfspool.clvr.locusdev.net/ifs/locus/data/analysis_results/100000",
        '''
        self.log.info('Building plan stats for all plans.')
        if self.ksess == None:  self._init_session()
        #
        # Determine the active plan from a different URL.
        active_planid = str(json.loads(self.ksess.get('/api/v1/plans/active').text)['id'])
        reply = json.loads(self.ksess.get('/api/v1/plans').text)
        for plan in reply:
            planid = str(plan['id'])
            planinfo = self.init(planid)
            group_ids_in_plan = []
            planinfo.update    ({ 'name'        : str(plan['name'])      })
            planinfo.update    ({ 'run_status'  : str(plan['runStatus']) })
            if planid == active_planid : 
                planinfo.update({ 'active'      : True                   })
            for group in plan['groups']:
                groupid = str(group['id'])
                group_ids_in_plan.append(groupid)
            planinfo.update     ({ 'group_ids'   : group_ids_in_plan      })
        return reply
#
#=============================================================================#
