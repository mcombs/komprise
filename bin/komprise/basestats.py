#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/basestats.py
#=============================================================================#
#
import os
import sys
import copy
from . import log, config, session
#
#=============================================================================#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-3])
#
#=============================================================================#
class BaseStats(object):
    '''Komprise statistics.  Extrapolated out in order to manage stats for 
multiple components.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        get_allowed_keys       : Return a list of allowed subkeys for the
                                 dictionary.
        show_structure         : Show the dictionary structure of the
                                 child class.
        init                   : Initialize the internal memory data structure.
        get_dict               : Get the dictionary meeting optional
                                 key='value' restrictions.  No restrictions
                                 returns the entire dictionary.
        get                    : Get the specfic value for dict[key][subkey].

    '''
    #
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO', struct_name=None, initial_values=None, allowed_values=None):
        self.debug = debug
        self.loglevel = loglevel.upper()
        if self.debug == True: loglevel='DEBUG'
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # All stats generally require a session with a Komprise
        # director which could be managed by this base class.
        self.ksess = None
        #
        # Initialize all the internal dictionary of dictionaries.
        self.struct_name = struct_name
        self.initial_values = initial_values
        self.allowed_values = allowed_values
        self.structure = {}
        return
    #
    #
    #----------------------------------------------------------------------#
    def _init_session(self):
        '''Session initiation gets its own function for simplicity's sake.
        '''
        self.log.info('Initializing session with Komprise director.')
        cfg = config.Config(debug=self.debug)
        cfg.read_config()
        self.ksess = session.Session(debug=self.debug, loglevel=self.loglevel, config=cfg)
        self.ksess.connect()
        return
    #
    #
    #-------------------------------------------------------------------------#
    def get_allowed_keys(self):
        return(self.allowed_keys.keys())
    #
    #
    #-------------------------------------------------------------------------#
    def show_structure(self):
        printout = '{}\nStructure of "{}"\n'.format('='*77,self.struct_name)
        printout += '\n{:<20} : {:<}\n'.format('Primary Key', (self.struct_name.upper() + 'ID'))
        printout += '\n{:<20s}  {}\n'.format('Value', 'Type')
        printout += '{:<20s}  {}\n'.format('-'*20, '-'*30)
        for k in self.initial_values.keys():
            printout += '{:<20s}  {}\n'.format(k, type(self.initial_values[k])) 
        print('{}'.format(printout))
        return
    #
    #
    #-------------------------------------------------------------------------#
    def init(self, key=None):
        '''Initilize the given data structure.

        Args:
           key        (REQ: str)   : Dictionary key being initialized.

        Returns:
           None.

        '''
        if key == None:
            raise AttributeError('Key must be specified.')
        self.log.debug('Initializing {} dictionary for key {}.'.format(self.struct_name, key))
        key= str(key)
        #
        # Warn if we have already initialized this value in the dictionary.
        if key in self.structure.keys():
            self.log.warning('ID "{}" already exists in {}. Re-initializing.'.format(key, self.struct_name))
        self.structure[key] = copy.deepcopy(self.initial_values)
        return(self.structure[key])
    #
    #
    #-------------------------------------------------------------------------#
    def get_dict(self, **kwargs):
        '''Pull sets from the full dictionary for a given structure with 
optional limits.

        Args:
           key='value'             : Key/Value pair.  Allowed keys and
                                     values are defined within init().

        Returns:
           List of volumes meeting criteria.

        '''
        self.log.debug('Getting dictionary {} with restrictions "{}".'.format(self.struct_name, kwargs))
        #
        # If no specific data requested, return all data for all ids.
        if len(kwargs.keys()) == 0: return(self.structure)
        #
        # If restrictions are requested, further check that the restrictions 
        # fall within the requested data.
        for k in kwargs.keys():
            if self.initial_values:
                if k not in self.initial_values.keys():
                    raise AttributeError('Key word "{}" not allowed for {} data structure.  Allowed keywords are "{}".'.format(k, self.struct_name, list(self.initial_values.keys())))
            if self.allowed_values:
                if k in self.allowed_values.keys() and kwargs[k] not in self.allowed_values[k]:
                    raise AttributeError('Requested value "{}" out of range for key "{}".  Allowed values are "{}".'.format(kwargs[k], k, self.allowed_values[k]))
            if type(kwargs[k]) != type(self.initial_values[k]):
                raise TypeError('Requested value "{}" is type "{}" but should be type "{}".'.format(kwargs[k], type(kwargs[k]), type(self.initial_values[k])))
        #
        # Otherwise pull the specific stats requested.
        subset = {}
        for structid in self.structure.keys(): 
            for k in kwargs.keys():
                if self.structure[structid][k] == kwargs[k]:
                    subset[structid] = self.structure[structid]
        return subset
    #
    #
    #-------------------------------------------------------------------------#
    def get(self, keyid=None, param=None):
        '''Generic function for pulling stats from the data structures.

        Args:
           keyid      (REQ: str)   : Key ID within the data structure -
                                     volumeid for volumes, etc.

           param      (REQ: str)   : Parameter being queried.

        Returns:
           Value requested.

        '''
        self.log.debug('Get {}["{}"]["{}"].'.format(self.struct_name, keyid, param))
        if keyid == None:
            raise AttributeError('ID must be specified.')
        keyid = str(keyid)
        if keyid not in self.structure.keys():
            raise AttributeError('ID "{}" unknown in {}.'.format(keyid, struct))
        if param == None:
            raise AttributeError('Requested parameter must be specified.')
        param = param.lower()
        if param not in self.initial_values.keys():
            raise AttributeError('Parameter "{}" unknown for {}.  Allowed keys are : "{}".'.format(param, self.struct_name, self.initial_values.keys()))
        return self.structure[keyid][param]
#
#
#=============================================================================#
