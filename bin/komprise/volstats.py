#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/volstats.py
#=============================================================================#
#
import os
import sys
import re
import json
from . import log, basestats
#
#=============================================================================#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-3])
#
#-----------------------------------------------------------------------------#
# Volume statistics.
VOl_SOURCE_TYPES = { 
    'run_data'         : re.compile(r'.*/locus/data/run_data/.*'),
    'analysis_results' : re.compile(r'.*/locus/data/analysis_results/.*'),
    'pgs'              : re.compile(r'.*/locus/data/pgs/.*'),
    'bp_archive'       : re.compile(r'/ifs/archive/.*'),
    'other'            : None,
}
# VolumeID is the key.
VOL_INIT_STATS = {
    'filerid'          : 0,
    'server'           : '',
    'path'             : '',
    'analyzed'         : False,
    'status'           : '',
    'error_details'    : '',
    'run_status'       : '',
    'in_plan'          : False,
    'copy_targets'     : [],
}
VOL_ALLOWED_VALUES = { 
    'analyzed'         : [ True, False ],
    'in_plan'          : [ True, False ],
}
#
#
#=============================================================================#
class VolStats(basestats.BaseStats):
    '''Komprise volume statistics.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level to use.
        session  (OPT: class)  : Session class with Komprise.

    Methods:
        build_volumes          : Build volume stats.

    '''
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO', session=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Initialize the parent statistics class.  The following are
        # handled here:
        #
        #  - Komprise director session.
        super().__init__(debug=self.debug, 
                         loglevel=self.loglevel,
                         struct_name='volume',
                         initial_values=VOL_INIT_STATS, 
                         allowed_values=VOL_ALLOWED_VALUES
                         )
        #
        # Confirm whether we were passed session information or will we
        # need to get a session with Komprise.
        if session:
            try:
                self.ksess = session
                self.ksess.get('/api/v1/dashboard/summary')
            except:
                self._init_session()
        return
    #
    #
    #-------------------------------------------------------------------------#
    def build_volumes(self, filerid=None):
        '''Build volume stats for a given filer.

        Args:
           filerid    (REQ: str)   : Filer ID for which to build a volume.

        Returns:
           None.

        #---------------------------------------------------------------------#
        Parsing: https://invitae.komprise.com/api/v1/datastore/filer/FILERID

          "volumes": [
            {
              "nfsHostname": "nfspool.clvr.locusdev.net",
              "nfsPath": "/ifs/locus/data/run_data",
              "id": "1146",
              "analyzed": true,        <----- Needs to be true
              "runStatus": "PAUSED",
              "status": "ERROR",
              "errorDetails": "Unable to verify share. Mount timed out for nfspool.clvr.locusdev.net:/ifs/locus/data/references",
              "copyTargets": [         <----- Needs to be len > 0
                "1215"
              ],
        '''
        if filerid == None or len(filerid) == 0:
            raise AttributeError('Filer name missing.')
        self.log.info('Building volume stats for filer ID "{}".'.format(filerid))
        if self.ksess == None:  self._init_session()
        reply = json.loads(self.ksess.get('/api/v1/datastore/filer/' + filerid).text)
        '''
        '''
        for volume in reply['volumes']:
            #
            # Initialize the volume ID and then add the values.
            volid = str(volume['id'])
            volinfo = self.init(volid)
            volinfo.update    ({ 'filerid'       : int(filerid)                 })
            volinfo.update    ({ 'server'        : str(volume['nfsHostname'])   })
            volinfo.update    ({ 'path'          : str(volume['nfsPath'])       })
            volinfo.update    ({ 'analyzed'      : bool(volume['analyzed'])     })
            volinfo.update    ({ 'status'        : str(volume['status'])        })
            volinfo.update    ({ 'error_details' : str(volume['errorDetails'])  })
            volinfo.update    ({ 'run_status'    : str(volume['runStatus'])     })
            volinfo.update    ({ 'copy_targets'  : volume['copyTargets']        })
            if len(volume['copyTargets']) > 0:
                volinfo.update({ 'in_plan'       : True                         })
        return
#
#=============================================================================#
