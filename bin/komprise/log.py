#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/komprise/log.py
#=============================================================================#
#
import sys
import logging
#
#=============================================================================#
class Log(object):
    '''Logging.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Level for logging.

    '''
    #
    #
    #----------------------------------------------------------------------#
    def __init__(self, debug=False, loglevel='INFO'):
        '''Assumes loglevel = 'critical|error|warning|info|debug'
        '''
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        levels = [ 'CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG' ]
        if self.loglevel not in levels:
            self.loglevel='WARNING'
        return
    #
    #
    #----------------------------------------------------------------------#
    def get(self):
        log = logging.getLogger(__name__)
        formatter_basic = logging.Formatter(fmt='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S %z')
        #formatter_debug = logging.Formatter(fmt='DEBUG: [%(asctime)s] %(filename)s(%(process)d) %(levelname)s [%(name)s.%(funcName)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S %z')
        formatter_debug = logging.Formatter(fmt='DEBUG: [%(asctime)s] %(filename)s(%(process)d) %(levelname)s [%(pathname)s:%(funcName)s()] %(message)s', datefmt='%Y-%m-%d %H:%M:%S %z')
        sh = logging.StreamHandler(stream=sys.stderr)
        if self.debug:
            sh.setFormatter(formatter_debug)
        else:
            sh.setFormatter(formatter_basic)
        if not log.handlers:
            log.setLevel(getattr(logging, self.loglevel))
            log.addHandler(sh)
        return(log)
#
#
#=============================================================================#
