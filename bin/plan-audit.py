#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/plan-audit.py
#=============================================================================#
#
import sys
import os
import argparse
import json
import re
import komprise
#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-2])
#
#=============================================================================#
def main():
    parser = argparse.ArgumentParser(description='Audit Komprise plan.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
    parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
    args = parser.parse_args()
    s = PlanAudit(args=args)
    s.setup()
    print('{}'.format(s.print()))
    return(s.exit_status)
#
#=============================================================================#
class PlanAudit(object):
    '''Create Komprise symlinks to their mapped id number.

    Parameters:
       debug        (OPT: bool)    : Enable debug mode. Sets log level to 'debug'.
       loglevel     (OPT: str)     : Set logging level
    '''
    def __init__(self, args=None):
        self.exit_status = 0
        self.debug = args.debug
        self.loglevel = args.loglevel.upper()
        mylog = komprise.log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Variables associated with the Komprise.
        self.planstats = None          # <-- Plan stats
        self.groupstats = None         # <-- Group stats
        self.volstats = None           # <-- Volume stats
        return
    #
    #
    #-------------------------------------------------------------------------#
    def setup(self):
        '''Set up dependencies before running.
        '''
        komp_sess = komprise.session.Session(debug=self.debug, loglevel=self.loglevel)
        komp_sess.connect()
        self.volstats = komprise.volstats.VolStats(debug=self.debug, loglevel=self.loglevel, session=komp_sess)
        self.volstats.build_volumes(filerid='1009')
        self.planstats = komprise.planstats.PlanStats(debug=self.debug, loglevel=self.loglevel, session=komp_sess)
        reply = self.planstats.build_plans()
        self.groupstats = komprise.groupstats.GroupStats(debug=self.debug, loglevel=self.loglevel, session=komp_sess)
        self.groupstats.build_groups(reply=reply)
        return
    #
    #
    #-------------------------------------------------------------------------#
    def print(self):
        '''Print the results of the audit.
        Args:
           None.

        Returns:
           None.
        '''
        printout = '{}\n'.format('='*77)
        printout += '{}\n\n'.format('Plans')
        for planid in self.planstats.get_dict().keys():
            printout += '{:<20s} : {:<}\n'.format('Name', self.planstats.get(planid, 'name'))
            printout += '{:<20s} : {:<}\n'.format('Plan ID', planid)
            printout += '{:<20} : {:<}\n'.format('Is Active', str(self.planstats.get(planid, 'active')))
            printout += '{:<20} : {:<}\n'.format('Run Status', str(self.planstats.get(planid, 'run_status')))
            printout += '{:<20s} :\n'.format('Group IDs')
            printout += '\n'
            printout += '    {:<10s}  {:<30s}  {:<10s}  {:<20s}  {:<}:{}\n'.format('Group ID', 'Group Name', 'Vol ID', 'Volume Status', 'Server', 'Path')
            printout += '    {:<10s}  {:<30s}  {:<10s}  {:<20s}  {:<}\n'.format('-'*10, '-'*30, '-'*10, '-'*20, '-'*40)
            for groupid in self.planstats.get(planid, 'group_ids'):
                groupname = str(self.groupstats.get(groupid, 'name'))
                for volid in self.groupstats.get(groupid, 'volume_ids'):
                    volserver = self.volstats.get(volid, 'server')
                    volpath = self.volstats.get(volid, 'path')
                    volstatus = self.volstats.get(volid, 'run_status')
                    printout += '    {:<10s}  {:<30s}  {:<10s}  {:<20s}  {:<}:{}\n'.format(str(groupid), groupname, str(volid), volstatus, volserver, volpath)
            printout += '\n{}\n'.format('='*77)
        return printout
#
#
#=============================================================================#
if __name__ == '__main__':  sys.exit(main())

