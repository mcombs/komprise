#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/make-vol-symlinks.py
#=============================================================================#
#
import sys
import os
import argparse
import json
import re
import komprise
#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-2])
TOP_TARGET = '/locus_komprise_s3_copy'
RE_ARCHIVE = re.compile(r'^/ifs/archive/.*', re.IGNORECASE)
#
#=============================================================================#
def main():
    parser = argparse.ArgumentParser(description='Analyze Komprise volumes..', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
    parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
    parser.add_argument('--noop', action='store_true', default=False, help='Perform no operation.')
    parser.add_argument('--force', action='store_true', default=False, help='Overwrite existing links.')
    args = parser.parse_args()
    s = MakeSymlinks(args=args)
    s.setup()
    #
    # Now sanity test permissions.
    s.sanity_tests()
    s.build_links()
    s.check_sources()
    if args.noop == True:
        s.print_links()
    else:
        s.make_links()
    return(s.exit_status)
#
#=============================================================================#
class MakeSymlinks(object):
    '''Analyze Komprise volumes.

    Parameters:
       debug        (OPT: bool)    : Enable debug mode. Sets log level to 'debug'.
       loglevel     (OPT: str)     : Set logging level
       noop         (OPT: bool)    : Do nothing.
       force        (OPT: bool)    : Force symlink creation even if it already exists.
    '''
    def __init__(self, args=None):
        self.exit_status = 0
        self.debug = args.debug
        self.loglevel = args.loglevel.upper()
        mylog = komprise.log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        self.noop = args.noop
        self.force = args.force
        #
        # Variables associated with the Komprise.
        self.komp_cfg = None           # <-- Config class
        self.komp_sess = None          # <-- Session class
        self.filer = None              # <-- Filer stats class
        self.vol = None                # <-- Volume stats class
        #
        # Internal variables.
        self.cfg = None                # <-- Configuration
        self.vols_in_plan = None       # <-- Volumes in plan.
        self.links = {}                # <-- Symlinks to create.
        return
    #
    #
    #-------------------------------------------------------------------------#
    def setup(self):
        '''Set up dependencies before running.  This should be run AFTER
logging has been set up.
        '''
        self.log.info('Establishing session to Komprise.')
        self.komp_cfg = komprise.config.Config(debug=self.debug, loglevel=self.loglevel)
        self.cfg = self.komp_cfg.read_config()
        self.komp_sess = komprise.session.Session(debug=self.debug, loglevel=self.loglevel, config=self.cfg)
        self.komp_sess.connect()
        self.cfg['password'] = '**REDACTED**'
        self.filer = komprise.filerstats.FilerStats(debug=self.debug, loglevel=self.loglevel, session=self.komp_sess)
        self.filer.build_filers()
        filerid = list(self.filer.get_dict(ipaddress='nfspool.clvr.locusdev.net').keys())[0]
        self.vol = komprise.volstats.VolStats(debug=self.debug, loglevel=self.loglevel, session=self.komp_sess)
        self.vol.build_volumes(filerid=filerid)
        self.vols_in_plan = self.vol.get_dict(in_plan=True)
        return
    #
    #
    #-------------------------------------------------------------------------#
    def sanity_tests(self):
        '''Run sanity tests.

        Args:
           args       (REQ: class) : An argparse class of arguments.

        Returns:
           None.

        '''
        self.log.info('Running sanity tests.')
        if not os.access(TOP_TARGET, os.W_OK):
            raise PermissionError('No write permission to create symlinks in "{}".'.format(TOP_TARGET))
        return
    #
    #
    #-------------------------------------------------------------------------#
    def _clean_dir(self, dirpath=None):
        '''Clean the link directory.

        Args:
           dirpath    (REQ: str)   : Pathname.

        Returns:
           None.

        '''
        newdir = TOP_TARGET
        # Skip /locus/data/analysis_results or /locus/data/run_data
        if re.match(RE_ARCHIVE, dirpath): return None
        if dirpath == '/ifs/locus/data/analysis_results':
            newdir += '/' + '/'.join(dirpath.split('/')[3:]) + '_pre90k'
        elif dirpath == '/ifs/locus/data/run_data':
            newdir += '/' + '/'.join(dirpath.split('/')[3:]) + '_pre10k'
        else:
            newdir += '/' + '/'.join(dirpath.split('/')[3:])
        self.log.debug('Cleaned "{}" to "{}".'.format(dirpath, newdir))
        return newdir
    #
    #
    #-------------------------------------------------------------------------#
    def build_links(self):
        '''Build the links to create. For ln source will be the copy_targets 
and target will be the original directory in /locus/data.

        Args:
           None.

        Returns:
           None.

        '''
        self.log.info('Building list of volumes for creating symlinks.')
        for sourceid in self.vols_in_plan.keys():
            linktarget = self._clean_dir(self.vol.get(sourceid, 'path'))
            if linktarget == None: continue
            #
            # For now, take only the first copy target.
            targetid = self.vol.get(sourceid, 'copy_targets')[0]
            linksource = '/kt/' + targetid + '/' + sourceid
            self.links[linksource] = linktarget
        return
    #
    #
    #-------------------------------------------------------------------------#
    def check_sources(self):
        '''Confirm that the source directories exist on this system.

        Args:
           None.

        Returns:
           None.

        '''
        self.log.info('Checking sources.')
        for linksource in self.links.keys():
            if not os.path.exists(linksource):
                self.log.warning('Source path "{}" does not exist.  Removing it.'.format(linksource))
                self.exit_status += 1
                del self.links[linksource]
            if not os.path.isdir(linksource):
                self.log.warning('Source path "{}" is not a directory.  Removing it.'.format(linksource))
                self.exit_status += 1
                del self.links[linksource]
        return
    #
    #
    #-------------------------------------------------------------------------#
    def check_targets(self):
        '''Confirm that the target symlinks exist and that they are symlinks.

        Args:
           None.

        Returns:
           None.

        '''
        self.log.info('Checking target symlinks.')
        for linksource in self.links.keys():
            target = self.links[linksource]
            if os.path.exists(target) and os.path.issymlink(target) and self.force == False:
                self.log.warning('Target symlink "{}" already exists but "--force" not specified.  Skipping it.'.format(target))
                self.exit_status += 1
                del self.links[linksource]
            elif os.path.exists(target) and not os.path.issymlink(target):
                self.log.warning('Target "{}" is not a symlink.  Skipping it.'.format(target))
                self.exit_status += 1
                del self.links[linksource]
        return
    #
    #
    #-------------------------------------------------------------------------#
    def make_links(self):
        '''Create the syminks.

        Args:
           None.

        Returns:
           None.

        '''
        self.log.info('Creating symlinks.')
        for linksource in self.links.keys():
            symlink_target_dir = os.path.dirname(self.links[linksource])
            if not os.path.exists(symlink_target_dir):
                self.log.info('Creating directory "{}"'.format(symlink_target_dir))
                if self.noop == False:
                    os.makedirs(symlink_target_dir, mode=0o755, exist_ok=True)
            elif not os.path.isdir(symlink_target_dir):
                self.log.error('File exists but is not a directory!'.format(symlink_target_dir))
            self.log.info('Symlinking "{}" --> "{}".'.format(linksource, self.links[linksource]))
            if self.noop == False:
                # 
                # If the target already exists, remove it.  The checks of 
                # the target - does it exist, is it a symlink, was 
                # "--force" specified - were already handled in 
                # check_targets() method.
                if os.path.exists(self.links[linksource]):
                    sys.log.debug('Removing existing symlink "{}".'.format(self.links[linksource]))
                    os.remove(self.links[linksource])
                os.symlink(linksource, self.links[linksource])
        return
    #
    #
    #-------------------------------------------------------------------------#
    def print_links(self):
        '''Simply print the symlinks which will be created.

        Args:
           None.

        Returns:
           None.

        '''
        for linksource in self.links.keys():
            sys.stderr.write('"{}" --> "{}"\n'.format(linksource, self.links[linksource]))
        return
#
#
#=============================================================================#
if __name__ == '__main__':  sys.exit(main())

