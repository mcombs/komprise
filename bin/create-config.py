#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/create-config.py
#=============================================================================#
#
import sys
import argparse
import komprise
#
# Parse arguments.
parser = argparse.ArgumentParser(description='Create Komprise config.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('--force', action='store_true', default=False, help='Overwrite existing links.')
args = parser.parse_args()
#
# Set up logging.
mylog = komprise.log.Log(debug=args.debug, loglevel=args.loglevel.upper())
log = mylog.get()
#
# Set up session.
komp = komprise.config.Config(debug=args.debug, loglevel=args.loglevel)
log.info('Creating the following config file "{}".'.format(komp.filename))
komp.print_config()
komp.write_config(force=args.force)
