#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/test-config.py
#=============================================================================#
#
import sys
import komprise
#
komp = komprise.config.Config(debug=True)
komp.set_filename('blah.cfg')
komp.set(clustername='invitae.komprise.com')
komp.set(password='**ANOTHER REDACTED VALUE**')
komp.set(timeout=15)
komp.set(verify_ssl=True)
komp.print_config()
sys.exit(0)
