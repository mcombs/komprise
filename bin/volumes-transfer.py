#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/komprise/src/master/bin/volumes-transfer.py
#=============================================================================#
#
import sys
import os
import argparse
import json
import re
import komprise
#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-2])
#
#=============================================================================#
def main():
    parser = argparse.ArgumentParser(description='Start another transfer of a Komprise volume.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
    parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
    parser.add_argument('--noop', action='store_true', default=False, help='Perform no operation.')
    parser.add_argument('--force', action='store_true', default=False, help='Force transfer.')
    parser.add_argument('volids', action='store', nargs='+', default=False, help='Overwrite existing links.')
    args = parser.parse_args()
    s = VolumesTransfer(args=args)
    s.setup()
    #
    # Now sanity test the volume IDs passed to ensure they exist or 
    # whether they have already been analyzed.
    s.sanity_tests(args)
    s.start_transfer(args.volids)
    return(s.exit_status)
#
#=============================================================================#
class VolumesTransfer(object):
    '''Analyze Komprise volumes.

    Parameters:
       debug        (OPT: bool)    : Enable debug mode. Sets log level to 'debug'.
       loglevel     (OPT: str)     : Set logging level
       noop         (OPT: bool)    : Do nothing.
       force        (OPT: bool)    : Force transfer even if one has already happened.

    Methods:
       setup           : Setup connections to a Komprise observer.
       sanity_tests    : Run any sanity tests before starting work.
       start_transfer  : Start transfer of a series of volumes.

    '''
    #
    #
    #-------------------------------------------------------------------------#
    def __init__(self, args=None):
        self.exit_status = 0
        self.debug = args.debug
        self.loglevel = args.loglevel.upper()
        mylog = komprise.log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        self.noop = args.noop
        self.force = args.force
        self.volids = args.volids
        #
        # Variables associated with the Komprise.
        self.filer = None                  # <-- Filer class.
        self.vol = None                    # <-- Volume class.
        #
        # Internal variables.
        # https://invitae.komprise.com/api/v1/volume/analyze?async=false
        self.komp_sess = None              # <-- Komprise session
        self.volumes = {}
        return
    #
    #
    #-------------------------------------------------------------------------#
    def setup(self):
        '''Set up dependencies before running.
        '''
        self.log.info('Running setup')
        self.komp_sess = komprise.session.Session(debug=self.debug, loglevel=self.loglevel)
        self.komp_sess.connect()
        self.filer = komprise.filerstats.FilerStats(debug=self.debug, loglevel=self.loglevel, session=self.komp_sess)
        self.filer.build_filers()
        self.vol = komprise.volstats.VolStats(debug=self.debug, loglevel=self.loglevel, session=self.komp_sess)
        # Specify the exact filer otherwise stats for all filers are built.
        all_volumes = []
        for filerid in self.filer.get_dict().keys():
            self.vol.build_volumes(filerid=filerid)
            all_volumes.append(self.vol.get_dict())
        for v in all_volumes:
            for volid in v.keys():
                self.volumes[volid] = v[volid]
        return
    #
    #
    #-------------------------------------------------------------------------#
    def sanity_tests(self, args=None):
        '''Run sanity tests.

        Args:
           args       (REQ: class) : An argparse class of arguments.

        Returns:
           None.

        '''
        self.log.info('Running sanity tests.')
        for volid in args.volids:
            if volid not in self.volumes.keys():
                raise ValueError('Volume ID "{}" is not known by Komprise.'.format(volid))
        return
    #
    #
    #-------------------------------------------------------------------------#
    def start_transfer(self, volids=None):
        '''Start transfer of the volumes.

        Args:
           volids     (REQ: class) : List of volume IDs to analyze.

        Returns:
           None.

        https://invitae.komprise.com/api/v1/volume/86935/migrationRecrawl?forceAttributeUpdate=true

        '''
        msg = 'Initiating transfer of the following volumes:\n'
        msg += '{:<12s} {:<}\n'.format('Volume ID', 'Path')
        msg += '{:<12s} {:<}\n'.format('-'*12, '-'*40)
        urls = {}
        for volid in volids:
            path = '/api/v1/volume/' + volid + '/migrationRecrawl'
            if self.force == True:
                path += '?forceAttributeUpdate=true'
            else:
                path += '?forceAttributeUpdate=false'
            urls[volid] = path
            msg += '{:<12s} {:<}\n'.format(volid, path)
        self.log.info(msg)
        if self.noop == True:
            return
        for volid in urls.keys():
            self.komp_sess.post(path=urls[volid])
        return
#
#
#=============================================================================#
if __name__ == '__main__':  sys.exit(main())
